package restservicetests;

import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.filter.log.LogDetail;
import io.restassured.http.ContentType;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;

public class Specification {
    public static RequestSpecification requestSpecification(String baseUri, int port, ContentType contentType){
        return new RequestSpecBuilder()
                .setBaseUri(baseUri)
                .setPort(port)
                .log(LogDetail.ALL)
                .setContentType(contentType)
                .build();
    }
    public static ResponseSpecification responseSpecification(){
        return new ResponseSpecBuilder()
                .log(LogDetail.ALL)
                .build();
    }
    public static void initSpec(RequestSpecification requestSpecification, ResponseSpecification responseSpecification){
        RestAssured.requestSpecification=requestSpecification;
        RestAssured.responseSpecification=responseSpecification;
    }
}
