package restservicetests.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class UserRequest {

   private String firstName;
   private String middleName;
   private String lastName;
   private String login;
   private String email;
   private String password;
}
