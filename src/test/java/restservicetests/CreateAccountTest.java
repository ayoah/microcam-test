package restservicetests;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import restservicetests.model.AuthRequest;
import restservicetests.model.AuthResponse;
import restservicetests.model.UserRequest;
import restservicetests.model.UserResponse;

public class CreateAccountTest {
    @Test
    void greetingTest(){
        Specification.initSpec(Specification.requestSpecification("http://test-microcam.relex.ru",40000,ContentType.JSON),Specification.responseSpecification());
        AuthRequest adminReq= new AuthRequest("Adm1n!11","h_X___e$u#0",null);

        AuthResponse adminToken = RestAssured
                .given()
                .when()
                    .body(adminReq)
                    .post("login")
                .then()
                    .statusCode(200)
                .extract().response().as(AuthResponse.class);


        RestAssured
                .given()
                .when()
                .header("Authorization",adminToken.getTokenType()+' '+adminToken.getAccessToken())
                .get("greet")
                .then()
                .statusCode(200);

    }
    @Test
    void createAccountWithCorrectArgs(){
        Specification.initSpec(Specification.requestSpecification("http://test-microcam.relex.ru",40000,ContentType.JSON),Specification.responseSpecification());
        UserRequest user = new UserRequest("firstName","middleName","lastName","user55te8st"+Math.random()*10,"userowtest6@example.com","Usertest'f*23_1");
        UserResponse userResponse=RestAssured
                .given()
                .when()
                  .body(user)
                    .post("users")
                .then()
                .statusCode(201)
                .extract().response().as(UserResponse.class);
        assertNotNull(userResponse.getUuid());
    }
    @Test
    void createAccountWithIncorrectPassword(){
        Specification.initSpec(Specification.requestSpecification("http://test-microcam.relex.ru",40000,ContentType.JSON),Specification.responseSpecification());
        UserRequest user = new UserRequest("firstName","middleName","lastName","user4444444t","userowtest6@example.com","Ser3/'");
        //password less than 8 symbols
        RestAssured
                .given()
                .when()
                .body(user)
                .post("users")
                .then()
                .statusCode(400);
        //password without uppercase letter
        user.setPassword("ser3/'rtr");
        RestAssured
                .given()
                .when()
                .body(user)
                .post("users")
                .then()
                .statusCode(400);
        //password without lowercase letter
        user.setPassword("SER3/'RTR");
        RestAssured
                .given()
                .when()
                .body(user)
                .post("users")
                .then()
                .statusCode(400);
        //password without number
        user.setPassword("SERa/'RTR");
        RestAssured
                .given()
                .when()
                .body(user)
                .post("users")
                .then()
                .statusCode(400);
        //password without special character
        user.setPassword("SERaw526RTR");
        RestAssured
                .given()
                .when()
                .body(user)
                .post("users")
                .then()
                .statusCode(400);
    }
    @Test
    void createAccountWithIncorrectLogin(){
        Specification.initSpec(Specification.requestSpecification("http://test-microcam.relex.ru",40000,ContentType.JSON),Specification.responseSpecification());
        UserRequest user = new UserRequest("firstName","middleName","lastName","user515te8st","userowtest6@example.com","SER3/'Rtr");
        // already exist login
        RestAssured
                .given()
                .when()
                .body(user)
                .post("users")
                .then()
                .statusCode(409);
        // login less than 8 symbols
        user.setLogin("useru");
        RestAssured
                .given()
                .when()
                .body(user)
                .post("users")
                .then()
                .statusCode(400);
        // login with all spaces
        user.setLogin("          ");
        RestAssured
                .given()
                .when()
                .body(user)
                .post("users")
                .then()
                .statusCode(400);
    }

}
