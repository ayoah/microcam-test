package restservicetests;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import org.junit.jupiter.api.*;
import restservicetests.model.AuthRequest;
import restservicetests.model.AuthResponse;
import restservicetests.model.UserRequest;
import restservicetests.model.UserResponse;

import java.util.UUID;
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class DeleteAccountTest {
    UserRequest user;
    UserResponse uuid;
    AuthResponse userToken;
    @BeforeEach
    void setUp(){
        Specification.initSpec(Specification.requestSpecification("http://test-microcam.relex.ru",40000, ContentType.JSON),Specification.responseSpecification());
        this.user = new UserRequest("firstName","middleName","lastName","user43te8st","userowtest6@example.com","Usertest'f*23_1");
        this.uuid=RestAssured
                .given()
                .when()
                    .body(user)
                    .post("users")
                .then()
                    .statusCode(201)
                    .extract().response().as(UserResponse.class);

        AuthRequest userReq= new AuthRequest("user43te8st","Usertest'f*23_1",null);

        this.userToken = RestAssured
                .given()
                .when()
                .body(userReq)
                .post("login")
                .then()
                .statusCode(200)
                .extract().response().as(AuthResponse.class);
    }

    @AfterEach
    void cleanUser(){
        RestAssured
                .given()
                .pathParam("user_id",uuid.getUuid())
                .when()
                .header("Authorization",this.userToken.getTokenType()+' '+this.userToken.getAccessToken())
                .body(user)
                .delete("users/{user_id}")
                .then();
    }
    @Test
    void deleteAccountWithCorrectArgs(){
        RestAssured
                .given()
                .pathParam("user_id",uuid.getUuid())
                .when()
                .header("Authorization",this.userToken.getTokenType()+' '+this.userToken.getAccessToken())
                .body(user)
                .delete("users/{user_id}")
                .then()
                .statusCode(200);
    }
    @Test
    void deleteAccountWithCorrectUUIDAndInvalidToken(){
        RestAssured
                .given()
                .pathParam("user_id",uuid.getUuid())
                .when()
                .header("Authorization",this.userToken.getTokenType()+' ')
                .body(user)
                .delete("users/{user_id}")
                .then()
                .statusCode(401);
    }
    @Test
    void deleteAccountWithIncorrectUUID(){
        UUID id= UUID.randomUUID();
        RestAssured
                .given()
                .pathParam("user_id",id)
                .when()
                .header("Authorization",this.userToken.getTokenType()+' '+this.userToken.getAccessToken())
                .body(user)
                .delete("users/{user_id}")
                .then()
                .statusCode(404);
    }
}
